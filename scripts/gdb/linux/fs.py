#
# gdb helper commands and functions for Linux kernel debugging
#
#  filesystem tools
#
# Copyright (c) Aurelien Aptel, 2015
#
# Authors:
#  Aurelien Aptel <aaptel@suse.com>
#
# This work is licensed under the terms of the GNU GPL version 2.
#

import gdb
import os
import sys
import itertools

# from itertools doc/recipes:

def nth(iterable, n, default=None):
    "Returns the nth item or a default value"
    return next(itertools.islice(iterable, n, None), default)

def consume(iterator, n):
    "Advance the iterator n-steps ahead. If n is none, consume entirely."
    # Use functions that consume iterators at C speed.
    if n is None:
        # feed the entire iterator into a zero-length deque
        collections.deque(iterator, maxlen=0)
    else:
        # advance to the empty slice starting at position n
        next(itertools.islice(iterator, n, n), None)

# ---

from linux import utils

struct_dentry = utils.CachedType("struct dentry")
struct_dentry_t = struct_dentry.get_type()
struct_dentry_ptr_t = struct_dentry_t.pointer()
struct_cifs_rdelem = utils.CachedType("struct cifs_rdelem")
struct_cifs_rdelem_t = struct_cifs_rdelem.get_type()
struct_cifs_rdelem_ptr_t = struct_cifs_rdelem_t.pointer()


def type_eq(a, b):
    return a.__str__() == b.__str__()

def iter_list_head(head, struct, member):
    item_ptr_t = gdb.lookup_type(struct).pointer()
    p = utils.container_of(head['next'], item_ptr_t, member)
    while p[member].address != head.address:
        yield p
        p = utils.container_of(p[member]['next'], item_ptr_t, member)

def cifs_rd_list():
    for sbi,sb in enumerate(super_block_list()):
        if sb['s_id'].string() == 'cifs':
            gdb.write('sb %2d CIFS:\n'%sbi)
            head = (sb['s_fs_info'].cast(gdb.lookup_type('struct cifs_sb_info').pointer()))['rtdislist']
            rdlist = iter_list_head(head, 'struct cifs_rdelem', 'rdlist')
            for i,e in enumerate(rdlist):
                gdb.write('%2d "%s" dentry %s inode %s\n'%(i, e['rdname'].string(), e['rdentry'], e['rdinode']))

def super_block_list():
    return iter_list_head(gdb.parse_and_eval("super_blocks"), "struct super_block", "s_list")

def dentry_list(d, depth=0):
    if type_eq(d.type, struct_dentry_ptr_t):
        d = d.dereference()
    elif not type_eq(d.type, struct_dentry_t):
        raise gdb.GdbError('argument must be of type struct dentry [*] (is %s)'%d.type)

    gdb.write('{indent}dentry {ptr} name "{name}" inode {inode}\n'.format(indent='  '*depth,
                                                                          ptr=d.address,
                                                                          name=d['d_name']['name'].string(),
                                                                          inode=d['d_inode']))
    children = iter_list_head(d['d_subdirs'], "struct dentry", "d_child")
    for c in children:
        dentry_list(c, depth+1)
    return

class LxDentryList(gdb.Command):
    """Print a dentry"""

    def __init__(self):
        super(LxDentryList, self).__init__("lx-dentry-list", gdb.COMMAND_DATA,
                                           gdb.COMPLETE_EXPRESSION)

    def invoke(self, arg, from_tty):
        argv = gdb.string_to_argv(arg)
        if len(argv) != 1:
            raise gdb.GdbError("lx-dentry-list takes one argument")
        dentry_list(gdb.parse_and_eval(argv[0]))

LxDentryList()

class LxSBList(gdb.Command):
    """Print super_block list"""

    def __init__(self):
        super(LxSBList, self).__init__("lx-sb-list", gdb.COMMAND_DATA,
                                                     gdb.COMPLETE_EXPRESSION)

    def invoke(self, arg, from_tty):
        argv = gdb.string_to_argv(arg)
        if len(argv) > 0:
            raise gdb.GdbError("lx-dentry-list takes no argument")
        for i, sb in enumerate(super_block_list()):
            gdb.write('sb %3d id = %s\n' % (i, sb['s_id']))


LxSBList()


class LxRDList(gdb.Command):
    """Dump all cifs rdlists"""

    def __init__(self):
        super(LxRDList, self).__init__("lx-rd-list", gdb.COMMAND_DATA,
                                                     gdb.COMPLETE_EXPRESSION)

    def invoke(self, arg, from_tty):
        argv = gdb.string_to_argv(arg)
        cifs_rd_list()



LxRDList()


class SB(gdb.Function):
    """Get Nth struct super_block"""

    def __init__(self):
        super(SB, self).__init__("SB")

    def invoke(self, n):
        return nth(super_block_list(), n)

SB()
